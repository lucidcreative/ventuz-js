import { colorToRGBString } from '../../src/utils/colors'

import { inspect } from 'util'
import winston from 'winston'

describe('Color Utilities', () => {
	it('converts a color name string to comma separated R,G,B', () => {
		expect(colorToRGBString('0, 0, 0')).toBe('0, 0, 0')
		// ignore warning
		const oldWinstonLevel = winston.level
		winston.level = 'error'
		expect(colorToRGBString(undefined)).toBe('127,127,127')
		// restore winston level
		winston.level = oldWinstonLevel
		expect(colorToRGBString('white')).toBe('255,255,255')

	})
})
