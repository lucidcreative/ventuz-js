// mock axios
import mockAxios from 'axios'
import mockFetch from 'node-fetch'
import winston from 'winston'
import { VMS_PORT } from '../../src/constants'
import { httpRemotingGet } from '../../src/utils/transport'

jest.mock('axios')
jest.mock('node-fetch')

const testIp = 'x.x.x.x'

describe('Transport Functions', () => {
	beforeEach(() => {
		jest.clearAllMocks()
	})

	describe('httpRemotingGet', () => {
		const address = '/Ventuz/Info'
		const targetFn = mockAxios.get

		beforeEach(() => {
			(targetFn as unknown as jest.MockInstance<typeof mockAxios.get>).mockImplementation(() => Promise.resolve({ data: '' }))
		})
		it('uses mockAxios to make calls', async () => {
			await httpRemotingGet(testIp, address, [])
			expect(targetFn).toHaveBeenCalledTimes(1)
		})

		it("doesn't include dangling ? when queryArgs is empty array", async () => {
			(targetFn as unknown as jest.MockInstance<typeof mockAxios.get>).mockImplementationOnce(() => Promise.resolve({ data: '' }))
			await httpRemotingGet(testIp, address, [])
			expect(targetFn).toBeCalledWith(
				`http://${testIp}:${VMS_PORT}/remoting/1.0/Remoting4${address}`,
				{ headers: { 'Accept': 'application/json' } },
			)
		})

		it("doesn't include dangling ? when queryArgs is undefined", async () => {
			(targetFn as unknown as jest.MockInstance<typeof mockAxios.get>).mockImplementationOnce(() => Promise.resolve({ data: '' }))
			await httpRemotingGet(testIp, address, undefined)
			expect(mockAxios.get).toBeCalledWith(
				`http://${testIp}:${VMS_PORT}/remoting/1.0/Remoting4${address}`,
				{ headers: { 'Accept': 'application/json' } },
			)
		})

		it('rejects Promise when axios returns an error', async () => {
			(targetFn as unknown as jest.MockInstance<typeof mockAxios.get>).mockImplementationOnce(() => Promise.reject('http error'))
			expect.assertions(1)
			await expect(httpRemotingGet(testIp, '/')).rejects.toThrowError()
		})

		it('rejects Promise when Ventuz data returns an error', async () => {
			// set winston to silent
			const winstonLevel = winston.level
			winston.level = 'silent'

			const response = require('./data/VentuzError.response.json');
			(targetFn as unknown as jest.MockInstance<typeof mockAxios.get>).mockImplementationOnce(() => Promise.resolve(response))
			expect.assertions(1)
			await expect(httpRemotingGet(testIp, '/')).rejects.toThrowError()

			// restore winston's original level
			winston.level = winstonLevel
		})
	})
	describe('httpRemotingPost', () => {

	})
})
