import { DataItemType, SceneModel } from '../../src/types'
import { individualTransformations, parseVentuzJson } from '../../src/utils/parseVentuzJson'

import { inspect } from 'util'
import winston from 'winston'

const sceneShape: SceneModel = {
	type: 'a type',
	animations: [],
	displayName: 'test',
	dataModel: {
		type: 'a type',
		items: [],
		isEmpty: true,
		hasWriteableItems: false,
		hasWriteableProperties: false,
		hasValues: false,
	},
	dataModelScene: {
		type: 'a type',
		items: [],
		isEmpty: true,
		hasWriteableItems: false,
		hasWriteableProperties: false,
		hasValues: false,
	},
}

describe('Parse Scene Model', () => {
	const data = require('../vms/session/data/getSceneModel.response.json')
	it('parses correctly', () => {
		const parsed = parseVentuzJson(data)
		// console.log(inspect(parsed, false, 12));
		expect(parsed).toHaveProperty('animations')
		expect(parsed).toBeInstanceOf(Object)
		expect(parsed).toHaveProperty(['animations', '0', 'dataModel', 'items', '0', 'type'])
		const colors = parsed.animations[0].dataModel.items[1].items.filter((item) => item.type === DataItemType.Color)
		colors.map((color) => {
			expect(color.default).toMatch(/\d+, ?\d+, ?\d+/)
		})
		// NOTE: in order to matchShapeOf, need to prototype whole datamodel
		// expect(parsed).toMatchShapeOf(sceneShape);
	})
})

describe('Make individual transformations', () => {
	it('converts color names to (R,G,B) and undefined to (127,127,127)', () => {
		const oldWinston = winston.level
		winston.level = 'silent'
		const testData = {
			key: 'items', value: [
				{ type: DataItemType.Color, default: 'white' },
				{ type: DataItemType.Color, default: '20,30,40' },
				{ type: DataItemType.Color, default: undefined },
			],
		}
		const result = individualTransformations(testData)
		expect(result.value[0].default).toBe('255,255,255')
		expect(result.value[1].default).toBe('20,30,40')
		expect(result.value[2].default).toBe('127,127,127')
		winston.level = oldWinston
	})
})
