import { toMatchOneOf, toMatchShapeOf } from 'jest-to-match-shape-of'

console.log('adding toMatchShapeOf...')
expect.extend({
	toMatchOneOf,
	toMatchShapeOf,
})
