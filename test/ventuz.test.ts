/**
 * Ventuz Library tests
 * @author KBjordahl
 */
import { } from '../lib'
import { Constants } from '../src/index'
test('Port is set to 20404', () => {
	expect(Constants.VMS_PORT).toBe(20404)
})
