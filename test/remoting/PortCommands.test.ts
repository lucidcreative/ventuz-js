import { cue, play } from '../../src/remoting/PortCommands'
import { CueOptions } from '../../src/types'

// logging
import winston from 'winston'
winston.level = 'error'

// mock axios
import * as transport from '../../src/utils/transport'
jest.mock('../../src/utils/transport')

const mockedPost = transport.httpRemotingPost as unknown as jest.Mock
const mockedGet = transport.httpRemotingGet as unknown as jest.Mock


const testIp = 'x.x.x.x'
const testScene = 'zzzz'
const testPort = '00'
const testTemplateData = `{
	"@": "ventuz://templates/workspace%7Ckyle%7Ckyletemplatetest/Main/S2",
	"Text": {
		"Text": "Chewie"
	}
}`

describe('Scene Commands', () => {
	beforeEach(() => {
		jest.clearAllMocks()
	})

	describe('Play', () => {
		it('uses http POST for transport and sends correct data', async () => {
			mockedPost.mockReturnValueOnce(() => ({ id: 1 }))
			expect.assertions(3)
			await play(testIp, testScene, testPort, CueOptions.Normal, testTemplateData)
			expect(transport.httpRemotingGet).toHaveBeenCalledTimes(0)
			expect(mockedPost).toHaveBeenCalledTimes(1)
			// final arg is rawDataResponse
			expect(transport.httpRemotingPost).toHaveBeenCalledWith(
				testIp, `/${testScene}/${testPort}/Play`, [CueOptions.Normal, testTemplateData], true,
			)
		})

	})
	describe('Cue', () => {
		it('uses http POST for transport and sends correct data', async () => {
			const validResponse = [
				{
					'Message': 'Out',
					'Duration': 1.0,
				},
			]
			mockedPost.mockReturnValue(validResponse)
			expect.assertions(3)
			const response = await cue(testIp, testScene, testPort, CueOptions.Normal, testTemplateData)
			expect(mockedGet).toHaveBeenCalledTimes(0)
			expect(mockedPost).toHaveBeenCalledTimes(1)
			expect(mockedPost).toHaveBeenCalledWith(
				testIp, `/${testScene}/${testPort}/Cue`, [CueOptions.Normal, testTemplateData, response.guid],
			)
		})

		it('throws an error when ventuz returns a non-HTTP error', async () => {
			winston.level = 'silent'
			expect.assertions(1)
			const errObject = {
				ErrorCode: 400,
				Error: 400,
				Message: 'Template failed',
				Details: 'One or more errors occurred.\r\n Found invalid data while decoding.: Failed',
			}
			const err = Error(`${errObject.Message}: ${errObject.Details}`)
			mockedPost.mockReturnValue(errObject)
			expect(cue(testIp, testScene, testPort, CueOptions.Normal, testTemplateData)).rejects.toEqual(err)
		})

	})
})
