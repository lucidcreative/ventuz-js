import { getDataItem, setDataItem } from '../../../src/remoting/SceneCommands'

// mock axios
import * as mockTransport from '../../../src/utils/transport'
jest.mock('../../../src/utils/transport')

// jest.mock('axios');

const testIp = 'x.x.x.x'
const testScene = 'zzzz'
const testDataItem = 'Not.A.Real.Thing'


describe('DataItem', () => {
	beforeEach(() => {
		jest.clearAllMocks()
	})

	it('gets using http remoting', async () => {
		await getDataItem(testIp, testScene, testDataItem)
		expect(mockTransport.httpRemotingGet).toHaveBeenCalledTimes(1)
	})
	it('sets using http remoting', async () => {
		await setDataItem(testIp, testScene, testDataItem, 1)
		expect(mockTransport.httpRemotingGet).toHaveBeenCalledTimes(1)
	})
	it('receives a confirmation of setting', async () => {
		(mockTransport.httpRemotingGet as unknown as jest.Mock).mockImplementationOnce((ip, path, args) => {
			return Promise.resolve(`${args[0]}`)
		})
		await expect(setDataItem(testIp, testScene, testDataItem, 1)).resolves.toBe('1')
	})
})

