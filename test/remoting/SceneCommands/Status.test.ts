import { parseSceneStatusOsc } from '../../../src/remoting/SceneCommands/Status'

import { SceneStatusResponse } from '../../../src'

// mock axios
import * as mockTransport from '../../../src/utils/transport'
jest.mock('../../../src/utils/transport')

// jest.mock('axios');

const testIp = 'x.x.x.x'
const testScene = 'zzzz'
const testDataItem = 'Not.A.Real.Thing'

describe('Status (async)', () => {
	beforeEach(() => {
		jest.clearAllMocks()
	})

	describe('OSC packet parsing', () => {
		it('correctly parses all parts of the osc packet', () => {
			const result = parseSceneStatusOsc({
				address: '/ZUZQ/Status',
				args:
					[{ type: 'i', value: 0 },
					{ type: 'i', value: 0 },
					{ type: 's', value: 'animationdata' },
					{ type: 'i', value: 0 },
					{ type: 'i', value: 1 }],
			}) as SceneStatusResponse
			expect(result.status.isLoaded).toBeFalsy()
			expect(result.status.isValidated).toBeFalsy()
			expect(result.status.isInvalid).toBeFalsy()
			expect(result.status.hasError).toBeFalsy()
			expect(result.status.loadingProgress).toBeCloseTo(0)
			expect(result.status.validationProgress).toBe(0)
			expect(result.sceneIdentity).toBe('animationdata')
			expect(result.hostIID).toBe('0000')
			expect(result.hostPort).toBe(1)
		})

		it('handles loaded but not yet validated status', () => {
			const { status } = parseSceneStatusOsc({
				address: '/ZUZQ/Status',
				args:
					[{ type: 'i', value: 0 },
					{ type: 'i', value: 256 },
					{ type: 's', value: 'animationdata' },
					{ type: 'i', value: 0 },
					{ type: 'i', value: 1 }],
			}) as SceneStatusResponse
			expect(status.isLoaded).toBeTruthy()
			expect(status.loadingProgress).toBeCloseTo(255)

			expect(status.isValidated).toBeFalsy()
			expect(status.validationProgress).toBeCloseTo(0)

			expect(status.isInvalid).toBeFalsy()
			expect(status.hasError).toBeFalsy()
		})

		it('handles loaded and currently validating', () => {
			const { status } = parseSceneStatusOsc({
				address: '/ZUZQ/Status',
				args:
					[{ type: 'i', value: 0 },
					{ type: 'i', value: 0x107 },
					{ type: 's', value: 'animationdata' },
					{ type: 'i', value: 0 },
					{ type: 'i', value: 1 }],
			}) as SceneStatusResponse
			expect(status.isLoaded).toBeTruthy()
			expect(status.loadingProgress).toBeCloseTo(255)

			expect(status.isValidated).toBeFalsy()
			expect(status.validationProgress).toBeCloseTo(7)

			expect(status.isInvalid).toBeFalsy()
			expect(status.hasError).toBeFalsy()
		})

		it('handles loaded and validated status', () => {
			const { status } = parseSceneStatusOsc({
				address: '/ZUZQ/Status',
				args:
					[{ type: 'i', value: 0 },
					{ type: 'i', value: 0x300 },
					{ type: 's', value: 'animationdata' },
					{ type: 'i', value: 0 },
					{ type: 'i', value: 1 }],
			}) as SceneStatusResponse
			expect(status.isLoaded).toBeTruthy()
			expect(status.loadingProgress).toBeCloseTo(255)

			expect(status.isValidated).toBeTruthy()
			expect(status.validationProgress).toBeCloseTo(255)

			expect(status.isInvalid).toBeFalsy()
			expect(status.hasError).toBeFalsy()
		})
		it('handles unloaded status', () => {
			const { status } = parseSceneStatusOsc({
				address: '/ZUZQ/Status',
				args:
					[{ type: 'i', value: 0 },
					{ type: 'i', value: 0x80010000 },
					{ type: 's', value: 'animationdata' },
					{ type: 'i', value: 0 },
					{ type: 'i', value: 1 }],
			}) as SceneStatusResponse
			expect(status.isLoaded).toBeFalsy()
			expect(status.loadingProgress).toBeCloseTo(0)

			expect(status.isValidated).toBeFalsy()
			expect(status.validationProgress).toBeCloseTo(0)

			expect(status.isInvalid).toBeTruthy()
			expect(status.hasError).toBeFalsy()
		})

		it('handles load error status', () => {
			const { status } = parseSceneStatusOsc({
				address: '/ZUZQ/Status',
				args:
					[{ type: 'i', value: 0 },
					{ type: 'i', value: 0x80020000 },
					{ type: 's', value: 'animationdata' },
					{ type: 'i', value: 0 },
					{ type: 'i', value: 1 }],
			}) as SceneStatusResponse
			expect(status.isLoaded).toBeFalsy()
			expect(status.loadingProgress).toBeCloseTo(0)

			expect(status.isValidated).toBeFalsy()
			expect(status.validationProgress).toBeCloseTo(0)

			expect(status.isInvalid).toBeTruthy()
			expect(status.hasError).toBeTruthy()
		})
	})
})
