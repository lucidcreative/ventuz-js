import { getSceneModel } from '../../../src/remoting/SceneCommands'

// mock axios
import * as mockTransport from '../../../src/utils/transport'
jest.mock('../../../src/utils/transport')

// jest.mock('axios');

const testIp = 'x.x.x.x'
const testScene = 'zzzz'
const testDataItem = 'Not.A.Real.Thing'

describe('getSceneModel', () => {
	beforeEach(() => {
		jest.clearAllMocks()
	})
	beforeEach(() => {
		(mockTransport.httpRemotingGet as unknown as jest.Mock).mockImplementationOnce(
			() => Promise.resolve(require('./data/SceneModel.response.json')))
	})

	it('gets using http remoting', async () => {
		await getSceneModel(testIp, testScene)
		expect(mockTransport.httpRemotingGet).toHaveBeenCalledTimes(1)
	})
	it('gets hidden items', async () => {
		const model = await getSceneModel(testIp, testScene)
		expect(model).toHaveProperty('DataModel')
		expect(model).toBeInstanceOf(Object)
	})
})

