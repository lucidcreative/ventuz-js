import parseSocket from '../../src/remoting/parseSocket'
import {
	parseScenePortsOsc,
	parseSceneStatusOsc,
} from '../../src/remoting/SceneCommands'
jest.mock('../../src/remoting/SceneCommands')


describe('Parse Socket Function', () => {

	it('correctly identifies Scene status packets', () => {
		const sceneStatusPacket = {
			address: '/ZUZQ/Status',
			args:
				[{ type: 'i', value: 0 },
				{ type: 'i', value: 768 },
				{ type: 's', value: 'animationdata' },
				{ type: 'i', value: 0 },
				{ type: 'i', value: 1 }],
		}
		parseSocket(sceneStatusPacket)
		expect(parseSceneStatusOsc).toHaveBeenCalled()
	})

	it('correctly identified Scene port async packets', () => {
		const scenePortPacket = {
			address: '/BYXC/Ports',
			args:
				[
					{ type: 'i', value: 0 },
					// [{ type: 's', value: '' }, { type: 'i', value: 4 }],
				],
		}
		parseSocket(scenePortPacket)
		expect(parseScenePortsOsc).toHaveBeenCalled()
	})
})
