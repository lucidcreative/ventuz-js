import { ImageFormat } from '../../src'
import { info, snapshotPipe } from '../../src/remoting/PipeCommands'

// util
import { readFile, writeFileSync } from 'fs'
import { promisify } from 'util'
import winston from 'winston'

// mock transport

import * as mockTransport from '../../src/utils/transport'
const transportPath = '../../src/utils/transport'
jest.mock('../../src/utils/transport')

// test constants
// winston.level = 'debug';
const testIp = '10.0.7.92'
const testPipe = '0000'

describe('Pipe Commands', () => {
	beforeEach(() => {
		jest.clearAllMocks()
	})

	describe('Info', () => {
		it('uses httpRemotingGet', async () => {
			await info(testIp, testPipe)
			expect(mockTransport.httpRemotingGet).toHaveBeenCalledTimes(1)
		})

		it('Gets info from Ventuz', async () => {
			mockTransport.httpRemotingGet.mockImplementationOnce(
				() => { return Promise.resolve(require('./data/PipeCommands/Info.response.json')) },
			)
			const vzInfo = await info(testIp, testPipe)
			expect(vzInfo).toHaveProperty('Mode')
			expect(vzInfo).toHaveProperty('GpuShareHandle')
			expect(vzInfo).toHaveProperty('Width')
			expect(vzInfo).toHaveProperty('Height')
			expect(vzInfo).toHaveProperty('RateNum')
			expect(vzInfo).toHaveProperty('RateDen')
			expect(vzInfo).toHaveProperty('PixelFormat')
			expect(vzInfo).toHaveProperty('LayoutIID')
			expect(vzInfo).toHaveProperty('WarmingUp')
		})
	})

	describe('Snapshot Pipe', () => {

		// it('uses httpRemotingGet', async () => {
		//     winston.level = 'debug';
		//     await snapshotPipe(testIp, testPipe, ImageFormat.BMP, 64, 64);
		//     expect(mockTransport.httpRemotingGet).toHaveBeenCalledTimes(1);
		// });
		// it('Gets a buffer type', async () => {
		//     mockTransport.httpRemotingGet.mockImplementationOnce(
		//         require.requireActual(transportPath).httpRemotingGet
		//         // async () => {
		//         //     const promisedRead = promisify(readFile);
		//         //     const result = await promisedRead(__dirname + '/data/PipeCommands/ventuzSnapshot.bmp');
		//         //     return result.toString('ascii');
		//         // }
		//     );
		//     winston.level = 'debug';
		//     const imgBuffer = await snapshotPipe(testIp, testPipe, ImageFormat.BMP, 64, 64);
		//     writeFileSync(__dirname + '/data/PipeCommands/ventuzSnapshot.bmp', imgBuffer);
		//     expect(imgBuffer).toBeInstanceOf(Buffer);
		//     expect(imgBuffer).toHaveLength(16442);
		// });
	})
})
