import { getLogLevel, getVersion, info, setLogLevel, setVersion } from '../../src/remoting/GeneralCommands'

// util
import winston from 'winston'

// mock transport
import * as mockTransport from '../../src/utils/transport'
jest.mock('../../src/utils/transport')

const mockedRemotingGet = mockTransport.httpRemotingGet as unknown as jest.Mock

// test constants
// winston.level = 'debug';
const testIp = 'x.x.x.x'

describe('General Commands', () => {
	beforeEach(() => {
		jest.clearAllMocks()
	})

	describe('Info', () => {
		it('uses httpRemotingGet', async () => {
			await info(testIp)
			expect(mockTransport.httpRemotingGet).toHaveBeenCalledTimes(1)
		})
		it('Gets info from Ventuz', async () => {
			mockedRemotingGet.mockImplementationOnce(
				() => { return Promise.resolve(require('./data/GeneralCommands/Info.response.json')) },
			)
			const vzInfo = await info(testIp)
			expect(vzInfo).toHaveProperty('Ventuz.Version')
			expect(vzInfo).toHaveProperty('Machine.Pipes')
			expect(vzInfo).toHaveProperty('Project')
		})
	})

	describe('LogLevel', () => {
		it('Gets using httpRemotingGet', async () => {
			await getLogLevel(testIp)
			expect(mockTransport.httpRemotingGet).toHaveBeenCalledTimes(1)
		})

		it('Gets LogLevel', async () => {
			mockedRemotingGet.mockImplementationOnce(
				() => { return '0' },
			)
			await expect(getLogLevel(testIp)).resolves.toBe('0')
		})

		it('Sets using httpRemotingGet', async () => {
			await setLogLevel(testIp, 1)
			expect(mockTransport.httpRemotingGet).toHaveBeenCalledTimes(1)
		})

		it('Sets the requested log level and returns it to confirm', async () => {
			mockedRemotingGet.mockImplementationOnce((ip, path, args) => { return `${args[0]}` })
			await expect(setLogLevel(testIp, 1)).resolves.toBe('1')
		})
	})

	describe('Version', () => {
		it('Gets using httpRemotingGet', async () => {
			await getVersion(testIp)
			expect(mockTransport.httpRemotingGet).toHaveBeenCalledTimes(1)
		})

		it('Gets the protocol version number', async () => {
			mockedRemotingGet.mockImplementationOnce(() => { return '2' })
			await expect(getVersion(testIp)).resolves.toBe('2')
		})

		it('Sets a protocol version and receives the change', async () => {
			mockedRemotingGet.mockImplementationOnce((ip, path, args) => { return `${args[0]}` })
			await expect(setVersion(testIp, 1)).resolves.toBe('1')
		})
	})
})
