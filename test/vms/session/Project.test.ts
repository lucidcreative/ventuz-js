import winston from 'winston'
import { getProjectById, getProjectList, getSceneModel, getSceneNamesByProjectId, start } from '../../../src/vms/session/project'

// mock transport
import * as mockTransport from '../../../src/utils/transport'

jest.mock('../../../src/utils/transport')
const vmsGet = mockTransport.vmsGet as unknown as jest.MockInstance<mockTransport.vmsGet>

// require('winston').level = 'debug';

const testIp = '10.0.7.92'
const testProject = '71a3774e-cef5-4343-8f4e-5290c1523658'
const testSceneName = 'animationdata'

const unmockVmsGet = () => {
	winston.level = 'debug'
	vmsGet.mockImplementationOnce(require.requireActual('../../../src/utils/transport').vmsGet)
}

describe('VMS Projects', () => {
	let winstonLevel: string
	beforeAll(() => { winstonLevel = winston.level })
	beforeEach(() => {
		jest.clearAllMocks()
	})

	afterEach(() => {
		winston.level = winstonLevel
	})

	describe('getProjectList', () => {
		const response = require('./data/getProjectList.response.json')
		beforeEach(() => {
			vmsGet.mockImplementation(() => Promise.resolve(response))

		})

		it('uses vmsGet for transport', async () => {
			await getProjectList(testIp)
			expect(vmsGet).toHaveBeenCalledTimes(1)
			expect(mockTransport.httpRemotingGet).toHaveBeenCalledTimes(0)
			expect(mockTransport.httpRemotingPost).toHaveBeenCalledTimes(0)
		})

		it('returns list of Projects', async () => {
			const projects = await getProjectList(testIp)
			expect(projects).toBeInstanceOf(Array)
			expect(projects[0]).toHaveProperty('id')
		})
	})

	describe('getProjectById', () => {
		const response = require('./data/getProjectById.response.json')
		beforeEach(() => {
			vmsGet.mockImplementation(() => Promise.resolve(response))
		})

		it('uses vmsGet for transport', async () => {
			await getProjectById(testIp, testProject)
			expect(vmsGet).toHaveBeenCalledTimes(1)
			expect(mockTransport.httpRemotingGet).toHaveBeenCalledTimes(0)
			expect(mockTransport.httpRemotingPost).toHaveBeenCalledTimes(0)
		})

		it('returns single project', async () => {
			const project = await getProjectById(testIp, testProject)
			expect(project).not.toBeInstanceOf(Array)
			expect(project).toHaveProperty('id')
		})
	})

	describe('getSceneNamesByProjectId', () => {
		const response = require('./data/getSceneNamesByProjectId.response.json')
		beforeEach(() => {
			vmsGet.mockImplementation(() => Promise.resolve(response))
		})

		it('uses vmsGet for transport', async () => {
			await getSceneNamesByProjectId(testIp, testProject)
			expect(mockTransport.vmsGet).toHaveBeenCalledTimes(1)
			expect(mockTransport.httpRemotingGet).toHaveBeenCalledTimes(0)
			expect(mockTransport.httpRemotingPost).toHaveBeenCalledTimes(0)
		})

		it('returns an array of scene name strings', async () => {
			const scenes = await getSceneNamesByProjectId(testIp, testProject)
			expect(scenes).toBeInstanceOf(Array)
			scenes.map((scene) => {
				expect(typeof scene).toBe('string')
				expect(scene).not.toContain('/')
				expect(scene).toMatch(/\w+(%7C\w+)*/)
			})

		})
	})

	describe('getSceneModel', () => {
		const response = require('./data/getSceneModel.response.json')
		beforeEach(() => {
			vmsGet.mockImplementation(() => Promise.resolve(response))
		})

		it('uses vmsGet for transport', async () => {
			await getSceneModel(testIp, testProject, testSceneName)
			expect(mockTransport.vmsGet).toHaveBeenCalledTimes(1)
		})

		it('correctly removes "%7C" instances from scene names', async () => {
			let getPath
			vmsGet.mockImplementationOnce((ip, path) => { getPath = path })
			const sceneModel = await getSceneModel(testIp, testProject, 'folder%7Cscene')
			expect(getPath).not.toMatch('%7C')
		})

		it('returns the SceneModel object of the selected scene', async () => {
			const sceneModel = await getSceneModel(testIp, testProject, testSceneName)
			expect(sceneModel.displayName).toBe(testSceneName)
			expect(sceneModel).toHaveProperty('dataModel')
		})
	})

	describe('start', () => {
		it('uses vmsGet for transport', async () => {
			await start(testIp, testProject)
			return expect(vmsGet).toHaveBeenCalledTimes(1)
		})

		it('assembles the correct url', async () => {
			await start(testIp, testProject)
			return expect(vmsGet).toHaveBeenCalledWith(
				testIp,
				`vms_session_service/1.0/projects/${testProject}/action/start`,
				[undefined, undefined],
				false,
			)
		})

	})
})
