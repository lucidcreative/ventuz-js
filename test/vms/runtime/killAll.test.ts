import { killAll } from '../../../src/vms/runtime/killAll'

// mock transport
import * as mockTransport from '../../../src/utils/transport'

jest.mock('../../../src/utils/transport')
const vmsGet = mockTransport.vmsGet as unknown as jest.Mock

describe('VMS killAll', () => {
	it('returns the number from the text response', async () => {
		vmsGet.mockImplementationOnce(() => 'Killed [3] processes')
		const result = await killAll('1.1.1.1')
		expect(result).toBe(3)
	})
	it('returns zero without error', async () => {
		vmsGet.mockImplementationOnce(() => 'Killed [0] process')
		const result = await killAll('1.1.1.1')
		expect(result).toBe(0)
	})
	it('throws an error on unexpected response', async () => {
		const badResult = 'Killed a ton of process'
		vmsGet.mockImplementationOnce(() => badResult)
		return expect(killAll('1.1.1.1')).rejects.toThrow(RegExp(badResult))
	})
})
