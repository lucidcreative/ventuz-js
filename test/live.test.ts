/**
 * Live tests
 * @author KBjordahl
 * These tests use .env vars for setup.
 * If you want to set LIVE_JSON_TEMPLATE, it must be escaped with querystring.escape()
 */
import * as querystring from 'querystring'
import winston from 'winston'
import { CueOptions, ImageFormat, Remoting, VMS } from '../src'

import { doScan } from '../src/vms/session/projectScanner'


// .env variables
require('dotenv').config()

if (process.env.LIVE_TESTS && JSON.parse(process.env.LIVE_TESTS.toLowerCase())) {
	winston.level = 'debug'
	const testIp = process.env.LIVE_IP || '192.168.7.3'
	const testScene = process.env.LIVE_IID || '0000'
	const testPort = process.env.LIVE_PORT || '00'
	const testTemplateData = process.env.LIVE_JSON_TEMPLATE ?
		querystring.unescape(process.env.LIVE_JSON_TEMPLATE) :
		'{"@": "ventuz://templates/workspace%7Ckyle%7Ckyletemplatetest/Main/S2","Text": {"Text": "live-test"}}'

	winston.warn(`.env LIVE_TESTS set to true, running live tests on ${testIp}::${testScene}/${testPort} with template data: ${testTemplateData}`)

	// describe('Live Template Port Tests', () => {
	//     // beforeEach (() => {
	//     //     mockAxios.get.mockClear();
	//     // });

	//     describe('Play', () => {
	//         beforeAll(async () => {
	//             await play(testIp, testScene, testPort, CueOptions.Deactivate, '');
	//         });
	//         afterAll(async () => {
	//             await play(testIp, testScene, testPort, CueOptions.Deactivate, '');
	//         });
	//         it('triggers live Ventuz instance', async () => {
	//             const playResult = await play(testIp, testScene, testPort, CueOptions.None, testTemplateData);
	//             expect(playResult).toBeTruthy();
	//         });
	//     });
	// });


	describe('Live VMS Tests', () => {
		describe('Project Scanner', () => {
			describe('Scan Action', () => {
				it('Returns a valid string', async () => {
					const result = await doScan(testIp)
					winston.info(`result was ${result}`)
					return expect(result).toBeTruthy()
				})
			})
		})
		// describe('Projects', () => {
		// 	describe('Start Project Action', () => {
		// 		it('Starts the first project found', async () => {
		// 			const projects = await VMS.Session.Project.getProjectList(testIp)
		// 			const result = await VMS.Session.Project.start(testIp, projects[0].id)
		// 		})
		// 	})
		// })
	})

	// describe('Live Pipe Tests', () => {
	// 	describe('Snapshot', () => {
	// 		it('Returns a Buffer', async () => {
	// 			winston.level = 'debug'
	// 			const imageBuffer = await Remoting.PipeCommands.snapshotPipe(testIp, testScene, ImageFormat.BMP, 64, 64)
	// 			expect(imageBuffer).toBeInstanceOf(Buffer)
	// 		})
	// 	})
	// })


} else {
	it('Skipping live tests', () => { })
}
