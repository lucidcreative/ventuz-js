
'use strict'
/**
 * @name Take (/<IID>/<PI>/Take)
 * @function take
 * @description **Type.** Response
 * @author KBjordahl
 * @param {string} ip: IP address of Ventuz
 * @param {string} iid: Host scene to take the cue in
 * @param {string} portId: port ordinal in the scene
 * @param {ByteArray} cudId: A 16-byte string that contains the Guid to identify the cued transition.
 */

// osc async = False

import { httpRemotingGet } from '../../utils/transport'

export async function take(ip: string, iid: string, portId: string, cueId: string) {
	const path = `/${iid}/${portId}/Take`
	const queryArgs = [
		cueId,
	]
	const response = await httpRemotingGet(ip, path, queryArgs)
	return response
}

export default { take }
