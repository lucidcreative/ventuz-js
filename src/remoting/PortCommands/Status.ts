
'use strict'
/**
 * @name Status (/<IID>/<PI>/Status)
 * @function status
 * @description **Get/Set scene and/or active state of a port.** Gets the current port status or sets the port's
 * scene assignment and/or activate/deactivates a port. The Port is addressed as a part of the OSC-Address. See Port Index
 * @author KBjordahl
 * @param {string} ip: IP address of Ventuz
 * @param {string} hostIid: the scene ID of the host scene
 * @param {string} portId: the port ordinal in the host scene
 * @param {string} childIid: If specified sets the port, or if not, clears the port
 * @param {Boolean} active: If specified activates/deactivates the port
 */

// osc async = False

import { httpRemotingGet } from '../../utils/transport'

export async function status(ip: string, hostIid: string, portId: string, childIid?: number, active?: Boolean) {
	const path = `/${hostIid}/${portId}/Status`
	const queryArgs = [
		childIid,
		active,
	]
	const response = await httpRemotingGet(ip, path, queryArgs)
	return response
}

export default { status }
