
'use strict'
/**
 * @name Cue (/<IID>/<PI>/Cue)
 * @function cueT
 * @description **Cues a Template-Transtion based on the current state.** The Cue command prepares a template for
 * being activated or deactivated based on the current status. The transition is immediately initialized and ready to be taken by the Take command.
 * @author KBjordahl
 * @param {string} ip: IP address of Ventuz
 * @param {string} iid: scene ID in which to load the template
 * @param {string} portId: port ordinal in which to load the template
 * @param {number} options: Cue option flags
 * @param {string} data: The Template-Data to be cued/uncued. If data is null or empty all active templates on the addressed port are uncued.
 * @param {string} cueId: GUID to identify the Cue for further Take commands. Will be auto-generated if not provided.
 */

// osc async = False

import { v4 } from 'uuid'
import { CueOptions } from '../../types'
import { httpRemotingPost } from '../../utils/transport'

// util
import * as R from 'ramda'

// logging
import winston from 'winston'

export interface CueResponse {
	guid: string
	takes: Take[]
}

export interface Take {
	message: string
	duration: number
}

export async function cue(
	ip: string, iid: string, portId: string, options: CueOptions, data?: string, cueId?: string,
): Promise<any> {

	const guid = cueId || v4()

	const path = `/${iid}/${portId}/Cue`
	const postArgs = [
		options,
		data,
		guid,
	]
	try {
		const response = await httpRemotingPost(ip, path, postArgs)

		winston.debug('/IID/PID/Cue: Ventuz sent:', response)

		if (R.has('Error', response)) {
			winston.error('Ventuz server returned an error: ', response)
			throw Error(`${response.Message}: ${response.Details}`)
		} else {
			return { guid, takes: response }
		}
	} catch (err) {
		winston.error('Error issuing cue command:\n', err)
		// console.error('ERROR::', err);
		// console.error(err);
		throw (err)
	}
}

export default { cue }
