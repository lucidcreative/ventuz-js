
'use strict'
/**
 * @name DataItem (/<IID>/<PI>//<DataItem>)
 * @function dataItem
 * @description **Get or sets the value of a Property of a Scene that is currently assigned to a Port.**
 * The <DataItem> part of the address is the full path of the data item within the scene. It follows the addressing
 * rules described in Template Engine . Please note async responses uses always the DataModel-qualified addressing!
 * @author KBjordahl
 * @param {string} ip: IP address of Ventuz
 * @param {string} iid:
 * @param {string} pid:
 * @param {string} dataItemAddress: scene address to data item
 * @param {any} value: All supported types are allowed. If specified and a Property is addressed the value of that
 * property is set. If no value is specified the property value is queried and attached to the response.
 * A required type conversion is performed when necessary using the invariant culture when converting from or into
 * strings. If a trigger is addressed the optional value is interpreted as int and the value is attached to the trigger.
 * A trigger value of zero is the default.
 */

// osc async = False

import { httpRemotingGet } from '../../utils/transport'

export async function dataItem(ip: string, iid: string, pid: string, dataItemAddress: string, value: any) {
	const path = `/${iid}/${pid}//${dataItemAddress}`
	const queryArgs = [
		value,
	]
	const response = await httpRemotingGet(ip, path, queryArgs)
	return response
}

export default { dataItem }
