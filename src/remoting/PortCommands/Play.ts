
'use strict'
/**
 * @name Cue (/<IID>/<PI>/Cue)
 * @function cueT
 * @description **Issues compound Cue+Take Commands** Only available via HTTP Remoting
 * @author KBjordahl
 * @param {string} ip: IP address of Ventuz
 * @param {string} iid: scene ID in which to load the template
 * @param {string} portId: two digit port ordinal in which to load the template
 * @param {number} options: Cue option flags
 * @param {string} data: The Template-Data to be cued/uncued. If data is null or empty all active templates on the addressed port are uncued.
 * @param {string} cueId: GUID to identify the Cue for further Take commands. Will be auto-generated if not provided.
 */

// not available on OSC

import { CueOptions, Errors } from '../../types'
import { httpRemotingPost } from '../../utils/transport'

// util
import * as R from 'ramda'

import winston from 'winston'

export async function play(
	ip: string, iid: string, portId: string, options: CueOptions, data: string = '',
): Promise<Boolean> {

	const path = `/${iid}/${portId}/Play`
	const postArgs = [
		options,
		data,
	]
	try {
		const response = await httpRemotingPost(ip, path, postArgs, true)

		const throwError = (error: Errors.VentuzError) => {
			winston.error('Ventuz server returned an error: ', error)
			throw (`${error.Message}: ${error.Details}`)
		}

		winston.info('/IID/PID/Play: Ventuz sent:', response)

		R.compose(
			R.ifElse(
				R.propEq('Error'),
				throwError,
				R.identity,
			),
		)(response)
		return true
	} catch (err) {
		winston.error('Error issuing play command:\n', err)
		// console.error('ERROR::');
		// console.error(err);
		throw (err)
	}
}

export default { play }
