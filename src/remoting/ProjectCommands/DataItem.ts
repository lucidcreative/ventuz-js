
'use strict'
/**
 * @name DataItem (/Ventuz//<DataItem>)
 * @function dataItem
 * @description **Gets or sets the value of a Project Data Property, calls a Project Trigger..**
 * The <DataItem> part of the address is the full path of the data item within the scene.
 * It follows the addressing rules described in Template Engine . Project Data doesn't support nested
 * data sets (sub-templates). Therefore a project data address always starts with a leading dot ( '.' )
 * @author KBjordahl
 * @param {string} ip: IP address of Ventuz
 * @param {string} dataItem: DataItem from Project Data (uses . notation)
 * @param {any} value: All supported types are allowed. If specified and a Property is addressed the value of
 * that property is set. If no value is specified the property value is queried and attached to the response.
 * A required type conversion is performed when necessary using the invariant culture when converting from or
 * into strings. If a trigger is addressed the optional value is interpreted as int and the value is attached
 * to the trigger. A trigger value of zero is the default.
 */

// osc async = False

import { httpRemotingGet } from '../../utils/transport'

async function _dataItem(ip: string, dataItem: string, value?: any): Promise<any> {
	const path = `/Ventuz//.${dataItem}`
	const queryArgs = [
		value,
	]
	const response = await httpRemotingGet(ip, path, queryArgs)
	return response
}

export async function getDataItem(ip: string, dataItem: string) {
	return await _dataItem(ip, dataItem)
}

export async function setDataItem(ip: string, dataItem: string, value: any) {
	return await _dataItem(ip, dataItem, value)
}

export default {
	setDataItem,
	getDataItem,
}
