
'use strict'
/**
 * @name Batch (/Ventuz/Batch)
 * @function batch
 * @description **Starts or commits/cancels a batch of set DataItem requests..** Batches combines multiple
 * set DataItem requests into one single atomic operation that is either committed or canceled. The first request
 * (no argument) is used to create a BatchID for the current connection (session). The returned BatchID is a negative
 * number used as timestamp for bundled requests that sets any DataItems. Finally the batch is comitted or canceled
 * with a second Batch request along with the BatchID and the Commit parameters. All set commands cached on server side
 * are executed at the time of the second Batch request at the specified timestamp in the OscBundle.
 * @author KBjordahl
 * @param {string} ip: IP address of Ventuz
 * @param {any} batchID: If specified the batch to commit or cancel (see next parameter)
 * @param {any} commit: Only valid if the previous parameter BatchID is specified. True = commit, False = cancel
 */

// osc async = False

import { httpRemotingGet } from '../../utils/transport'

export async function batch(ip: string, batchID: any, commit: any) {
	const path = '/Ventuz/Batch'
	const queryArgs = [
		batchID,
		commit,
	]
	const response = await httpRemotingGet(ip, path, queryArgs)
	return response
}

export default { batch }
