
'use strict'
/**
 * @name Files (/Ventuz/Files)
 * @function files
 * @description **Get all files of a data pool.** Retrieve all files of a specified data pool.
 * A data pool usually refers to a sub folder with the Ventuz project folder - but it could be delegated
 * to another location on the local disk, a network folder or a database. Files returned on this request
 * contain the full relative path and the original file extension. The data pool Scenes doesn't return the
 * file extension so the returned scene files matches directly the scene identity.
 * @author KBjordahl
 * @param {string} ip: IP address of Ventuz
 * @param {FilePool} pool: The Data Pool to be queried as case insensitive string.
 */

// osc async = False

import { FilePool } from '../../types'
import { httpRemotingGet } from '../../utils/transport'

export async function files(ip: string, pool: FilePool) {
	const path = '/Ventuz/Files'
	const queryArgs = [
		pool,
	]
	const response = await httpRemotingGet(ip, path, queryArgs)
	return response
}

export default { files }
