
'use strict'
/**
 * @name SceneModel (/Ventuz/SceneModel)
 * @function sceneModel
 * @description **Get complete information about all data models and templates of a scene stored on disk.
 * ** Gets the entire XML SceneModel description on request. The SceneModel is read directly from disk.
 * The actual scene is not loaded into memory.
 * @author KBjordahl
 * @param {string} ip: IP address of Ventuz
 * @param {string} sceneIdentity: The Scene Identity (filename without extension) of the scene to be loaded.
 */

// osc async = False

import { httpRemotingGet } from '../../utils/transport'

export async function sceneModel(ip: string, sceneIdentity: string) {
	const path = '/Ventuz/SceneModel'
	const queryArgs = [
		sceneIdentity,
	]
	const response = await httpRemotingGet(ip, path, queryArgs)
	return response
}

export default { sceneModel }
