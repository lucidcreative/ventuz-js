
'use strict'
/**
 * @name Clock (/Ventuz/Clock)
 * @function clock
 * @description **Get the current renderer clock.** Get the current rendering time.
 * The returned value is the number of frames in its fps time base returned by /Ventuz/Info .
 * The returned time has no relation to the time of the day in reality! Please note that the returned value could
 * lie in the past at the time of reception! The FPS parameters defines the duration of a frame like this:
 * @author KBjordahl
 * @param {string} ip: IP address of Ventuz
 */

// osc async = False

import { httpRemotingGet } from '../../utils/transport'

export async function clock(ip: string) {
	const path = '/Ventuz/Clock'
	const response = await httpRemotingGet(ip, path)
	return response
}

export default { clock }
