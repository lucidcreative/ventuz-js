
'use strict'
/**
 * @name DataModel (/Ventuz/DataModel)
 * @function dataModel
 * @description **Gets the DataModel of the Project Data..** Request
 * @author KBjordahl
 * @param {string} ip: IP address of Ventuz
 */

// osc async = False

import { httpRemotingGet } from '../../utils/transport'

export async function dataModel(ip: string) {
	const path = '/Ventuz/DataModel'
	const response = await httpRemotingGet(ip, path)
	return response
}

export default { dataModel }
