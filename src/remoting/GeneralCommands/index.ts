export * from './Info'
// export * from './Log';
export * from './LogLevel'
export * from './Version'

import Info from './Info'
import LogLevel from './LogLevel'
import Version from './Version'

export default {
	...Version,
	...LogLevel,
	...Info,
}
