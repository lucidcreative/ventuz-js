
'use strict'
/**
 * @name LogLevel (/Ventuz/LogLevel)
 * @function logLevel
 * @description **Get or set log level.** Gets or sets the level of log messages
 * that the clients wants to receive asynchronously. The default log level for new sessions is None .
 * After a new connection is created the client should set the desired log level before issuing any other commands.
 * @author KBjordahl
 * @param {string} ip: IP address of Ventuz
 * @param {number} logLevel: Sets the log level if specified. Otherwise query the current log level.
 */

// osc async = False

import { httpRemotingGet } from '../../utils/transport'

export async function getLogLevel(ip: string) {
	const path = '/Ventuz/LogLevel'
	const response = await httpRemotingGet(ip, path)
	return response
}

export async function setLogLevel(ip: string, logLevel: number) {
	const path = '/Ventuz/LogLevel'
	const queryArgs = [
		logLevel,
	]
	const response = await httpRemotingGet(ip, path, queryArgs)
	return response
}

export default {
	getLogLevel,
	setLogLevel,
}
