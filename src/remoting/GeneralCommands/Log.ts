
'use strict'
/**
 * @name Log (/Ventuz/Log)
 * @function logasync
 * @description **Receive log message, response only.** A Log message is
 * received whenever Ventuz logs an entry which matches the log level set by the LogLevel command.
 * @author KBjordahl
 * @param {string} ip: IP address of Ventuz
 * @param {number} requestID: Always 0 .
 * @param {number} logLevel: Log level of this message.
 * @param {any} mod: Module reference to the program module that caused that log entry or NULL.
 * @param {any} message: Message text of that entry or NULL.
 */

// osc async = True

// import { httpRemotingGet } from '../../utils/transport';

// export default async function log(ip: string, requestID: number, logLevel: number, mod: any, message: any) {
//     const path = '/Ventuz/Log';
//     const queryArgs = [
//         requestID,
//         logLevel,
//         mod,
//         message
//     ];
//     // TODO: can't send via http. need to implement osc
//     return;
// }
