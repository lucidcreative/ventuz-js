
/**
 * @name Info (/Ventuz/Info)
 * @function infoasync
 * @description **Get general Ventuz process information.**
 * Retrieve general information about the running Ventuz server.
 * If the client is connected to a Ventuz Designer the Project ID may change during a session.
 * In this case the sever will send an asynchronous /Ventuz/Info message to notify the client
 * about a project change. A client should release all its scene references and states in such a case.
 * @author KBjordahl
 * @param {string} ip: IP address of Ventuz
 */

// osc async = True

import { httpRemotingGet } from '../../utils/transport'

export async function info(ip: string) {
	const path = '/Ventuz/Info'

	const response = await httpRemotingGet(ip, path)
	return response
}

export default { info }
