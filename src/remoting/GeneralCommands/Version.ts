
'use strict'
/**
 * @name Version (/Ventuz/Version)
 * @function version
 * @description **Get or set protocol version.** The Ventuz message is used to either determine
 * if the connected peer is a Ventuz server or to check which protocol version it supports.
 * The message itself only uses standard OSC types and is therefore upward compatible with all future versions.
 * A client should always try to downgrade a newer server implementation to the supported protocol version of the
 * client in order to work properly. The server will either confirm the downgrade with a proper response or reject
 * it with an error.
 * @author KBjordahl
 * @param {string} ip: IP address of Ventuz
 * @param {number} version: Sets the version if specified. Otherwise query the version only.
 */

// osc async = False

import { httpRemotingGet } from '../../utils/transport'

export async function getVersion(ip: string) {
	const path = '/Ventuz/Version'
	const response = await httpRemotingGet(ip, path)
	return response
}

export async function setVersion(ip: string, version: number) {
	const path = '/Ventuz/Version'
	const queryArgs = [
		version,
	]
	const response = await httpRemotingGet(ip, path, queryArgs)
	return response
}

export default { setVersion, getVersion }
