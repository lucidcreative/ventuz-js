
'use strict'
/**
 * @name Validate (/<IID>/Validate)
 * @function validate
 * @description **Validates a scene.** Validate the addressed IID. Status messages are received during the validation process.
 * @author KBjordahl
 * @param {string} ip: IP address of Ventuz
 * @param {string} iid: Scene ID to validate
 */

// osc async = False

import { httpRemotingGet } from '../../utils/transport'

export async function validate(ip: string, iid: string) {
	const path = `/${iid}/Validate`

	const response = await httpRemotingGet(ip, path)
	return response
}

export default { validate }
