
'use strict'
/**
 * @name Scenes (/<IID>/Scenes)
 * @function scenes
 * @description **Get loaded (in-memory) scenes.** Retrieve all scenes currently loaded in memory.
 * It optionally requests only scenes that has been loaded by the addressed IID or all scenes (global).
 * @author KBjordahl
 * @param {string} ip: IP address of Ventuz
 * @param {string} iid: Scene id to get loaded scenes from.
 * @param {Boolean} global: If set to true all scenes in memory are returned. If false only scene loaded by the addressed IID are returned.
 */

// osc async = False

import { httpRemotingGet } from '../../utils/transport'

export async function scenes(ip: string, iid: string, global?: Boolean) {
	const path = `/${iid}/Scenes`
	const queryArgs = [
		global,
	]
	const response = await httpRemotingGet(ip, path, queryArgs)
	return response
}

export default { scenes }
