
'use strict'
/**
 * @name Ports (/<IID>/Ports)
 * @function ports
 * @description **Get information about the ports of a scene.**
 * Enumerate the ports of the addressed scene. Asynchronous response is sent if
 * the ports of a scene have been changed. This usually only happens if the scene
 * is currently opened in Ventuz Designer and the ports are modified on the fly.
 * Scenes in the Ventuz Runtime never change their port list.
 * @author KBjordahl
 * @param {string} ip: IP address of Ventuz
 * @param {string} iid: Scene ID to get ports from
 */

// osc async = True

import { OscMessage, PortInfo, ScenePortsResponse } from '../../types'
import parseFlags from '../../utils/parseFlags'
import { httpRemotingGet } from '../../utils/transport'

export async function ports(ip: string, iid: string) {
	const path = `/${iid}/Ports`

	const response = await httpRemotingGet(ip, path)
	return response
}

export function parseScenePortsOsc(oscMsg: OscMessage): ScenePortsResponse {
	const addressParts = oscMsg.address.split('/')

	// set flags based on status array
	return {
		IID: addressParts[1]!,
		portInfo: (<any> oscMsg.args[1]).map((portArray: any[]): PortInfo => {
			const flags = parseFlags(portArray[1].value, 4)
			return {
				name: portArray[0].value || '',
				isLocked: flags[0] === '1',
				isManualTemplate: flags[0] === '2',
				isLayer: flags[0] === '4',
			}
		}),
	}
}

export default { ports }
