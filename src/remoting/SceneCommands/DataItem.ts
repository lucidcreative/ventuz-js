
'use strict'
/**
 * @name DataItem (/<IID>//<DataItem>)
 * @function dataItemTasync
 * @description **Get or sets the value of a Property, calls a Trigger or receives async triggers..**
 * The <DataItem> part of the address is the full path of the data item within the scene. It follows
 * the addressing rules described in Template Engine . Please note async response always uses the DataModel-qualified addressing!
 * @author KBjordahl
 * @param {string} ip: IP address of Ventuz
 * @param {string} iid: 4 character unique scene ID
 * @param {string} dataItem: the path to the data item
 * @param {any} value: All supported types are allowed. If specified and a Property is addressed the value of that
 * property is set. If no value is specified the property value is queried and attached to the response.
 * A required type conversion is performed when necessary using the invariant culture when converting from or
 * into strings. If a trigger is addressed the optional value is interpreted as int and the value is attached
 * to the trigger. A trigger value of zero is the default.
 */

// osc async = True

import { httpRemotingGet } from '../../utils/transport'

export async function setDataItem(ip: string, iid: string, dataItem: string, value: any) {
	const cleanedDataItem = dataItem.replace('/', '.')
	const path = `/${iid}//.${cleanedDataItem}`
	const queryArgs = [
		value,
	]
	const response = await httpRemotingGet(ip, path, queryArgs)
	return response
}

export async function getDataItem(ip: string, iid: string, dataItem: string) {
	const cleanedDataItem = dataItem.replace('/', '.')
	const path = `/${iid}//.${cleanedDataItem}`
	const response = await httpRemotingGet(ip, path)
	return response
}

export default {
	getDataItem,
	setDataItem,
}
