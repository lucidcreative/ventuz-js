/**
 * @name SceneModel (/<IID>/SceneModel)
 * @function sceneModelasync
 * @description **Get complete information about all data models and templates of a scene in memory.
 * ** Gets the entire XML SceneModel description on request. The response is also transferred
 * if the scene model has been modified by the user. This can only happen if connected to Ventuz Designer
 * and the operator is modifying the template structure or meta data.
 * @author KBjordahl
 * @param {string} ip: IP address of Ventuz
 * @param {string} iid: Scene ID to get SceneModel from
 */

// osc async = True

import winston from 'winston'
import { httpRemotingGet } from '../../utils/transport'

export async function getSceneModel(ip: string, iid: string) {
	const path = `/${iid}/SceneModel`
	try {
		const response = await httpRemotingGet(ip, path)
		return response
	} catch (error) {
		winston.error('Error in getSceneModel: ', error)
	}
}

export default {
	getSceneModel,
}
