
'use strict'
/**
 * @name Load (/<IID>/Load)
 * @function load
 * @description **Load a scene into memory.** Loads a scene into memory. Issuing a Load request always returns a valid IID.
 * Subsequent Status messages informs the client about the progress and success of the load. The addressed IID specifies the
 * scene that actually performs load. If a pipe is specified the currently layout scene of that pipe performs the load.
 * If a scene is specified that scene performs the load.
 * @author KBjordahl
 * @param {string} ip: IP address of Ventuz
 * @param {string} targetIid: IID of the scene to load the new scene beneath in the Scene Tree
 * @param {string} sceneIdentity: The Scene Identity (filename without extension) of the scene to be loaded.
 * @param {PortLoadFlags} loadFlags: Indicates how a scene is loaded.
 */

// osc async = False

import { httpRemotingGet } from '../../utils/transport'

import { Port } from '../../types'

export async function load(ip: string, targetIid: string, sceneIdentity: string, loadFlags: Port.LoadFlags): Promise<Port.LoadResponse> {
	const path = `/${targetIid}/Load`
	const queryArgs = [
		sceneIdentity,
		loadFlags,
	]
	const response = await httpRemotingGet(ip, path, queryArgs)
	return { iid: response.IID, isNew: response.IsNew }
}

export default {
	load,
}
