
'use strict'
/**
 * @name Status (/<IID>/Status)
 * @function status
 * @description **Get status of a scene.** Gets the status of a scene instance
 * specified by the IID in the address. Such messages are also sent asynchronously
 * without specific request - hence a client receives status changes of all scenes in memory!
 * @author KBjordahl
 * @param {string} ip: IP address of Ventuz
 * @param {string} iid: Scene ID to get status of
 */

// osc async = True

import { OscMessage, SceneStatusResponse } from '../../types'
import intToFourCC from '../../utils/fourCC'
import parseFlags from '../../utils/parseFlags'
import { httpRemotingGet } from '../../utils/transport'

export async function status(ip: string, iid: string) {
	const path = `/${iid}/Status`

	const response = await httpRemotingGet(ip, path)
	return response
}

export function parseSceneStatusOsc(oscMessage: OscMessage): SceneStatusResponse {
	const addressParts = oscMessage.address.split('/')
	const statusArray = parseFlags(oscMessage.args[1]!.value, 8)
	const progress = parseInt(`0x${statusArray[1]}${statusArray[0]}`, 16)
	const isLoaded = parseInt(statusArray[2], 10) >= 1
	const isValidated = parseInt(statusArray[2], 10) >= 3
	const hostIID = intToFourCC(parseInt(oscMessage.args[3].value, 10))
	// set flags based on status array
	return {
		IID: addressParts[1]!,
		status: {
			isLoaded,
			isValidated,
			loadingProgress: isLoaded ? 255 : progress,
			validationProgress: isValidated ? 255 : isLoaded ? progress : 0,
			isInvalid: statusArray[7] === '8',
			hasError: statusArray[4] === '2' && statusArray[7] === '8',
		},
		sceneIdentity: oscMessage.args[2].value,
		hostIID,
		hostPort: oscMessage.args[4].value,
	}
}

export default { status }
