
'use strict'
/**
 * @name Release (/<IID>/Release)
 * @function release
 * @description **Releases a loaded scene.** Releases the addressed IID.
 * Only scenes loaded by the remoting can be released. Releasing a scene doesn't
 * necessarily unload it from memory, because it still could be used by a scene port
 * or it is opened in the Ventuz Designer. One could try to remove the scene from
 * its port before issuing a Release command in order to unload it. Status message will
 * be sent if the scene gets unloaded due to a Release .
 * @author KBjordahl
 * @param {string} ip: IP address of Ventuz
 * @param {string} iid: Scene ID to release
 */

// osc async = False

import { httpRemotingGet } from '../../utils/transport'

export async function release(ip: string, iid: string) {
	const path = `/${iid}/Release`

	const response = await httpRemotingGet(ip, path)
	return response
}

export default { release }
