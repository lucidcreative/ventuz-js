
'use strict'
/**
 * @name DataItem (/Ventuz/Options//<DataItem>)
 * @function dataItem
 * @description **Gets or sets the value of a Live Option..** The <DataItem> part of the address is
 * the full path of the data item. Live options are always rooted and have no named DataModels.
 * Therefore the address of the DataItem always start with a '.'
 * @author KBjordahl
 * @param {string} ip: IP address of Ventuz
 * @param {any} value: All supported types are allowed. If specified and a Property is addressed the value
 * of that property is set. If no value is specified the property value is queried and attached to the response.
 * A required type conversion is performed when necessary using the invariant culture when converting from or
 * into strings. If a trigger is addressed the optional value is interpreted as int and the value is attached to
 * the trigger. A trigger value of zero is the default.
 */

// osc async = False

import { httpRemotingGet } from '../../utils/transport'

export async function dataItem(ip: string, value: any) {
	const path = '/Ventuz/Options//${DataItem{'
	const queryArgs = [
		value,
	]
	const response = await httpRemotingGet(ip, path, queryArgs)
	return response
}

export default { dataItem }
