
'use strict'
/**
 * @name DataModel (/Ventuz/Options/DataModel)
 * @function dataModel
 * @description **Gets the DataModel of all Ventuz Live Options such as Genlock delay, ShowPointer, etc.** Request
 * @author KBjordahl
 * @param {string} ip: IP address of Ventuz
 */

// osc async = False

import { httpRemotingGet } from '../../utils/transport'

export async function dataModel(ip: string) {
	const path = '/Ventuz/Options/DataModel'

	const response = await httpRemotingGet(ip, path)
	return response
}

export default { dataModel }

