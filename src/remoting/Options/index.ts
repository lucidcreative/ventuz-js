export * from './DataItem'
export * from './DataModel'

import DataItem from './DataItem'
import DataModel from './DataModel'

export default {
	...DataModel,
	...DataItem,
}
