import { OscMessage } from '../types'
// import SceneCommands from './SceneCommands'
import {
	parseScenePortsOsc,
	parseSceneStatusOsc,
} from './SceneCommands'

export default function parseSocketData(message: OscMessage) {
	const addressParts = message.address.split('/')
	addressParts.shift()
	switch (true) {
		// Ventuz namespace for general ventuz things
		case (addressParts[0].toLowerCase() === 'Ventuz'):
			switch (addressParts[1].toLowerCase()) {
				case 'info':
					return message
				default: break
			}
			break

		// scene command, which starts with scene IID (4 capital letters)
		case (/[A-Z]{4}/.test(addressParts[0])):
			switch (addressParts[1].toLowerCase()) {
				case 'status':
					return parseSceneStatusOsc(message)
				case 'ports':
					return parseScenePortsOsc(message)
				default: break
			}
			break
		default: break
	}
	return message
}
