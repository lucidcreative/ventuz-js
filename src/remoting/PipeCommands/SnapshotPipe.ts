
'use strict'
/**
 * @name Snapshot (/<PipeIID>/Snapshot)
 * CURRENTLY SNAPSHOT IS BROKEN. NEED TO REIMPLEMENT REQUEST WITH FETCH
 * @function snapshotPipe
 * @description **Get a screen-shot of a render pipe.** Gets a snapshot of the addressed pipe and
 * transferred as an image. Please note that this command is not real-time capable and the rendering may
 * stutter while grabbing an image!
 * @author KBjordahl
 * @param {string} ip: IP address of Ventuz
 * @param {string} pipeIID:
 * @param {number} imageFormat: The Image Format to be transferred.
 * @param {number} width: The image will be scaled down or up to the specified size (width and height in pixels).
 * A size of 0,0 will maintain the original size.
 * @param {number} height: The image will be scaled down or up to the specified size (width and height in pixels).
 * A size of 0,0 will maintain the original size.
 * @param {any} rectangle: Sub area of the image or 0,0,0,0 for the entire image
 * @param {number} filter: Filter to be used when scaling down the image.
 * @param {Boolean} alpha: If true the image will contain the alpha channel if the specified image format supports it)
 */

// osc async = False

import winston from 'winston'
import { ImageFilter, ImageFormat } from '../../types'
import { httpRemotingGet } from '../../utils/transport'

export async function snapshotPipe(
	ip: string,
	pipeIID: string,
	imageFormat: ImageFormat = ImageFormat.BMP,
	width: number = 0,
	height: number = 0,
	xStart: number = 0,
	xEnd: number = 0,
	yStart: number = 0,
	yEnd: number = 0,
	filter: ImageFilter = ImageFilter.None,
	alpha: Boolean = true): Promise<Buffer> {
	const path = `/${pipeIID}/Snapshot`
	const queryArgs = [
		imageFormat,
		width,
		height,
		xStart,
		xEnd,
		yStart,
		yEnd,
		filter,
		alpha,
	]

	try {
		const response = await httpRemotingGet(ip, path, queryArgs, true, { responseType: 'arraybuffer' })
		// response is an image file, so make a buffer out of it
		winston.debug(`Response is: ${response}`)
		const img = Buffer.from(response.data, 'binary')
		return img

	} catch (error) {
		throw Error(`Couldn't retrieve image! ${error}`)
	}
}

export default { snapshotPipe }
