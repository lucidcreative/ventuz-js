export * from './Info'
export * from './SnapshotPipe'

import Info from './Info'
import SnapshotPipe from './SnapshotPipe'

export default {
	...SnapshotPipe,
	...Info,
}
