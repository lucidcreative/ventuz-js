
'use strict'
/**
 * @name Info (/<PipeIID>/Info)
 * @function infoasync1
 * @description **Get information about a render pipe.** Retrieve the shared memory information
 * of a render pipe. Only render pipes that have the MemoryGPU flag are valid addresses for this request.
 * All other pipes will return a NotSupported error. A SharedMemoryInfo message is returned as an asynchronous
 * response if the share information has changed during runtime (device lost/reset, window resized, project closed, etc)
 * @author KBjordahl
 * @param {string} ip: IP address of Ventuz
 * @param {string} pipeIID: unique 4 character ID of the pipe
 */

// osc async = True

import { httpRemotingGet } from '../../utils/transport'

export async function info(ip: string, pipeIID: string) {
	const path = `/${pipeIID}/Info`
	const response = await httpRemotingGet(ip, path)
	return response
}

export default { info }
