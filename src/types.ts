export interface Project {
	author: string
	category: string
	copyright: string
	description: string
	fullPath: string
	id: string
	name: string
	thumbnailBase64: Buffer
	vpr: boolean
	ventuzVersion: string
}

export interface SceneModel {
	type: string
	animations?: Animation[]
	category?: any
	channelIds?: any
	dataModel: DataModel
	dataModelScene: DataModel
	description?: string
	displayName: string
	identity?: any
	keywords?: any
	thumbnail?: any
	thumbnailArea?: {
		height?: number;
		width?: number;
		x?: number;
		y?: number;
	}
}

export interface Animation {
	type: string
	animations?: Animation[]
	connections?: Connection[]
	dataModel: DataModel
	isGroup?: boolean
	name: string
	states: State[]

}

export interface ColorDefault {
	// should become an enum
	knownColor: number
	name: any
	// should become an enum
	state: number
	// should become an enum
	value: number
}

export interface Connection {
	dataPoint: number
	dataPointReverse: number
	duration: number
	from: number
	id: number
	name: string
	nameReverse: string
	routingDirection: number
	to: number
}

export interface DataModel {
	type: string
	items?: DataItem[]
	tag?: any
	isEmpty: boolean
	hasWriteableItems: boolean
	hasWriteableProperties: boolean
	hasValues: boolean
}

export interface DataItem {
	type: DataItemType
	dmxChannels: number
	description: string
	label: string
	mode: DataItemMode
	name: string
	tag: any
	userData: string
	// only in groups
	items?: DataItem[]
	default?: any
	max?: any
	min?: any
	speed?: any
	format?: string
	choiceSource?: any
	choices?: any
	maxLines: any
	minLines: any
	regEx: any
	alpha?: boolean
	elements?: string[]
	required?: boolean
	// this should become an enum eventutally
	assetPools: number
}

export interface State {
	category?: string
	channelIds?: string
	dataModel?: DataModel
	description?: string
	displayName?: string
	id: number
	keywords?: string
	name?: string
	thumbnail?: string
	thumbnailArea?: {
		height?: number;
		width?: number;
		x?: number;
		y?: number;
	}
	type: StateType
}

export enum DataItemMode {
	None = 'N',
	R = 'R',
	W = 'W',
	RW = 'RW',
}

export enum DataItemType {
	Group = 'Ventuz.Remoting4.SceneModel.Group, Ventuz.Kernel.Remoting4',
	Boolean = 'Ventuz.Remoting4.SceneModel.Boolean, Ventuz.Kernel.Remoting4',
	Float = 'Ventuz.Remoting4.SceneModel.Float, Ventuz.Kernel.Remoting4',
	Double = 'Ventuz.Remoting4.SceneModel.Double, Ventuz.Kernel.Remoting4',
	Integer = 'Ventuz.Remoting4.SceneModel.Integer, Ventuz.Kernel.Remoting4',
	Trigger = 'Ventuz.Remoting4.SceneModel.Trigger, Ventuz.Kernel.Remoting4',
	String = 'Ventuz.Remoting4.SceneModel.String, Ventuz.Kernel.Remoting4',
	Color = 'Ventuz.Remoting4.SceneModel.Color, Ventuz.Kernel.Remoting4',
	Enum = 'Ventuz.Remoting4.SceneModel.Enum, Ventuz.Kernel.Remoting4',
	Asset = 'Ventuz.Remoting4.SceneModel.Asset, Ventuz.Kernel.Remoting4',

}

export enum StateType {
	Normal = 'Normal',
	Begin = 'Begin',
	BeginDefault = 'BeginDefault',
	End = 'End',
	EndDefault = 'EndDefault',
	Presented = 'Presented',
	PresentedDefault = 'PresentedDefault',
}

// snapshot enums
export enum ImageFormat {
	BMP = 0,
	PNG,
	TIF,
	JPG,
}

export enum ImageFilter {
	None = 0,
	Low,
	High,
}

// cue enums
export enum CueOptions {
	Normal = 0,
	JumpOut = 1,
	JumpIn = 2,
	Snap = 3,
	Deactivate = 4,
}

export enum FilePool {
	Misc = 'misc',
	Project = 'project',
	Scenes = 'scenes',
	Images = 'images',
	Textures = 'textures',
	Audio = 'audio',
	Movies = 'movies',
	Data = 'data',
	Geometries = 'geometries',
	Shaders = 'shaders',
	Scripts = 'scripts',
	Fonts = 'fonts',
	Sounds = 'sounds',
	Documentation = 'documentation',
	Assets = 'assets',
}
export namespace Port {
	export enum LoadFlags {
		None = 0,
		New,
		Existing,
		Unused = 4,
	}

	export interface LoadResponse {
		iid: string
		isNew: boolean
	}
}
export namespace Errors {

	export enum ErrorTypes {
		NoProject,
		InvalidIID,
		InvalidPort,

		// Data Item Specific
		InvalidDataItem,
		InvalidArgument,
		UpdateFailed,

		// Port Command Specific
		CueTemplateFailed,
		TakeFailed,
		Failed,
		TransitionCanceled,
	}

	export interface VentuzError {
		Message: string
		Details: string
	}
}
export interface OscMessage {
	address: string,
	args: {
		type: string,
		value: any,
	}[],
}

export type VentuzInfoResponse = {
	version: string,
}

export type SceneStatusResponse = {
	IID: string,
	status: {
		isLoaded: boolean,
		loadingProgress: number,
		isValidated: boolean,
		validationProgress: number,
		hasError: boolean,
		isInvalid: boolean,
	},
	sceneIdentity: string,
	hostIID: string,
	hostPort: number,
}

export type PortInfo = {
	name?: string,
	isLocked: boolean,
	isManualTemplate: boolean,
	isLayer: boolean,
}

export type ScenePortsResponse = {
	IID: string,
	portInfo: (PortInfo | PortInfo[])[],
}
