import * as Constants from './constants'
import * as Types from './types'

import RemotingLib from './remoting'
import VMSLib from './vms'

import winston from 'winston'
try {
	winston.level = process.env.LOG_LEVEL || process.env.NODE_ENV === 'development' ? 'debug' : 'warn'
} catch {
	winston.level = process.env.NODE_ENV === 'development' ? 'debug' : 'warn'
}

export const Remoting = { ...RemotingLib }
export const VMS = { ...VMSLib }
export {
	Constants,
	Types,
}
