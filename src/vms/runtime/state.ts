'use strict'

import { vmsGet } from '../../utils/transport'

// utils
import winston from 'winston'



export async function state(ventuzIp: string): Promise<StateResponse> {
	// kill all runtimes on an instance
	const commandPath = 'vms_service/1.0/runtime/state'
	try {
		const response = await vmsGet(ventuzIp, commandPath)
		winston.debug(`state returns:\n${response}`)
		const parsedResponse = /^(.*?) \[(\d{8})\]: (.*)$/gmi.exec(response)
		if (parsedResponse && parsedResponse.length === 4) return {
			state: parsedResponse[1].toLowerCase() as 'ok' | 'error',
			flags: parsedResponse[2].split('').map(x => parseInt(x, 10)),
			message: parsedResponse[3],
		}
		else throw Error(`Unexpected response: ${response}`)
	} catch (err) {
		winston.error('ERROR in getStatus: ', err)
		throw (err)
	}
}

export default { state }

interface StateResponse {
	state: 'ok' | 'error',
	flags: number[],
	message: string
}
