'use strict'

import { vmsGet } from '../../utils/transport'

// utils
import winston from 'winston'



export async function killAll(ventuzIp: string): Promise<number> {
	// kill all runtimes on an instance
	const commandPath = 'vms_service/1.0/runtime/killall'
	try {
		const response = await vmsGet(ventuzIp, commandPath)
		winston.debug(`killAll returns:\n${response}`)
		const parsedResponse = /Killed \[(\d*)\] process/gmi.exec(response)
		if (parsedResponse && parsedResponse.length === 2) return parseInt(parsedResponse[1], 10)
		else throw Error(`Unexpected response: ${response}`)
	} catch (err) {
		winston.error('ERROR in getStatus: ', err)
		throw (err)
	}
}

export default { killAll }
