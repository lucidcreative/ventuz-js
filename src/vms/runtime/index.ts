import killAll from './killAll'
import state from './state'


export default {
	...killAll,
	...state,
}
