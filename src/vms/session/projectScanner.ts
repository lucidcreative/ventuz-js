'use strict'

import { vmsGet } from '../../utils/transport'

// utils
import * as R from 'ramda'
import { inspect } from 'util'
import winston from 'winston'

// import { Project, SceneModel } from '@/.';

// const
const rootPath = 'vms_session_service/1.0/projectscanner'

export async function getStatus(ventuzIp: string): Promise<string> {
	// Root path for projectScanner, returns the status of the project scanner
	const commandPath = rootPath
	try {
		const response = await vmsGet(ventuzIp, commandPath, [], false)
		winston.debug(`getStatus returns:\n${inspect(response)}`)
		return response
	} catch (err) {
		winston.error('ERROR in getStatus: ', err)
		throw (err)
	}

}

export async function doScan(ventuzIp: string): Promise<any> {
	const commandPath = R.join('/', [rootPath, 'action', 'scan'])
	try {
		const response = await vmsGet(ventuzIp, commandPath, [], false)
		winston.debug(`doScan returns:\n${inspect(response)}`)
		// check the response
		if (response.length === 0) { throw ('Did not receive a valid response') }

		// wait until status is 'scan completed'
		// this feels like a terrible way to do this...
		while (true) {
			const status = await getStatus(ventuzIp)
			if (status.toLowerCase().includes('scan completed')) break
		}
		return true
	} catch (err) {
		winston.error('ERROR in doScan: ', err)
		throw (err)
	}
}

export async function getProjectDetailsByFilepath(ventuzIp: string, fullpath: string): Promise<any> {
	const commandPath = R.join('/', [rootPath, 'action', 'projectdetails'])
	try {
		const response = await vmsGet(ventuzIp, commandPath, { fullpath }, false)
		// check the response
		if (response.length === 0) { throw (`Project at ${fullpath} does not have any scenes`) }
		// this one is a straight array, just return
		winston.debug(`getProjectDetailsByFilepath returns:\n${inspect(response)}`)
		const cleanedResponse = R.map(R.replace(/\//g, '%7C'), response)
		return cleanedResponse
	} catch (err) {
		winston.error('ERROR in getProjectDetailsByFilepath: ', err)
		throw (err)
	}
}

export default {
	getStatus,
	doScan,
	getProjectDetailsByFilepath,
}
