'use strict'

import { vmsGet } from '../../utils/transport'

// utils
import { camel } from 'change-case'
import { escape } from 'querystring'
import * as R from 'ramda'
import { inspect } from 'util'
import winston from 'winston'

import { Project, SceneModel } from '../../../src/types'
import { parseVentuzJson } from '../../utils/parseVentuzJson'

// const
const rootPath = 'vms_session_service/1.0/projects'
// wrapper functions to typecast
const camelProject = (obj: any): any => R.reduce((acc: any, key: any) => R.assoc(camel(key), obj[key], acc), {}, R.keys(obj)) // camelKeys(obj);


export async function getProjectList(ventuzIp: string): Promise<Project[]> {
	const commandPath = rootPath
	try {
		const response = await vmsGet(ventuzIp, commandPath, [], false)
		const renamed = R.map(camelProject, response)
		winston.debug(`getProjectList returns:\n${inspect(renamed)}`)
		return renamed
	} catch (err) {
		winston.error('ERROR in getProjectList: ', err)
		throw (err)
	}

}

export async function getProjectById(ventuzIp: string, projectId: string): Promise<Project> {
	const commandPath = R.join('/', [rootPath, projectId])
	try {
		const response = await vmsGet(ventuzIp, commandPath, [], false)
		// check the response
		if (response.length !== 1) { throw ('Did not receive a project') }
		// camel the keys in the response
		const renamed = camelProject(response[0])
		winston.debug(`getProjectsById returns:\n${inspect(renamed)}`)
		return renamed
	} catch (err) {
		winston.error('ERROR in getProjectById: ', err)
		throw (err)
	}
}

export async function getSceneNamesByProjectId(ventuzIp: string, projectId: string): Promise<string[]> {
	const commandPath = R.join('/', [rootPath, projectId, 'scenes'])
	try {
		const response = await vmsGet(ventuzIp, commandPath, [], false)
		// check the response
		if (response.length === 0) { throw (`Project ${projectId} does not have any scenes`) }
		// this one is a straight array, just return
		winston.debug(`getScenesByProjectId returns:\n${inspect(response)}`)
		const cleanedResponse = R.map(R.replace(/\//g, '%7C'), response)
		return cleanedResponse
	} catch (err) {
		winston.error('ERROR in getSceneNamesByProjectId: ', err)
		throw (err)
	}
}

export async function getSceneModel(ventuzIp: string, projectId: string, sceneName: string): Promise<SceneModel> {
	const commandPath = R.join('/', [
		rootPath,
		projectId,
		'scenes',
		escape(R.replace(/%7C/g, '/', sceneName)),
	])
	try {
		const response = await vmsGet(ventuzIp, commandPath, [], false)
		// this one is a straight json, camelize and return
		const renamed = parseVentuzJson(response)
		winston.debug(`getSceneModel returns:\n${inspect(response)}`)
		return renamed
	} catch (err) {
		winston.error('ERROR in getScenesByProjectId: ', err)
		throw (err)
	}
}

export async function start(ventuzIp: string, projectId: string, fullPath?: string, version?: string): Promise<string> {
	const commandPath = R.join('/', [
		rootPath,
		projectId,
		'action/start',
	])
	try {
		const response = await vmsGet(ventuzIp, commandPath, [fullPath, version], false)
		winston.debug(`startProject returns:\n${inspect(response)}`)

		return response
	} catch (err) {
		winston.error('ERROR in getScenesByProjectId: ', err)
		throw (err)
	}
}

export default {
	getProjectList,
	getProjectById,
	getSceneModel,
	getSceneNamesByProjectId,
	start,
}
