// types
import { DataItemType, SceneModel } from '../types'

// util
import { camel } from 'change-case'
import * as R from 'ramda'
import { colorToRGBString } from './colors'


export function parseVentuzJson(sceneModel: any): SceneModel {

	// const typeSafeCall = (action: () => any) => action();

	const perKey = ([oldKey, oldValue]: [any, any]): R.KeyValuePair<string, any> => {
		let value, key

		// function to use in recursing
		const handleValue = (individualValue: any): any => {
			if (Object.prototype.toString.call(individualValue) === '[object Array]') {
				return R.map(handleValue, individualValue)
			} else if (typeof individualValue === 'object') {
				const oldPairsInternal = R.toPairs(individualValue)
				const mappedPairsInternal = R.map(perKey, oldPairsInternal)
				return R.fromPairs(mappedPairsInternal)
			} else {
				return individualValue
			}
		}
		if (oldValue) {
			// handle the collection object layer from Ventuz JSON
			if (R.has('$values', oldValue)) {
				// make this an array from the $values field
				oldValue = oldValue['$values']
			}
			value = handleValue(oldValue)
		}

		key = camel(oldKey)

		const { key: transformedKey, value: transformedValue } = individualTransformations({ key, value })

		return [transformedKey, transformedValue]
	}

	const oldPairs = R.toPairs(sceneModel)
	const mappedPairs = R.map(perKey, oldPairs)
	const finalPairs = (): any => R.fromPairs(mappedPairs)
	return finalPairs()
}

// make a transformation of an individual key value pair
export function individualTransformations(kvPair: KeyValuePair): KeyValuePair {
	let { value: newValue, key: newKey } = kvPair
	switch (kvPair.key) {
		// items is a list of DataItems
		case 'items':
			// remapping defaults
			newValue = R.map(
				(item: any) => ({
					...item,
					default: item.type === DataItemType.Color ?
						colorToRGBString(item.default)
						: item.type === DataItemType.Boolean ?
							!!item.default
							: item.default,
				}),
				kvPair.value,
			)
			// this is redundant to get rid of the let vs const ts error - KB
			newKey = kvPair.key
			break

		default:
			break
	}

	return { key: newKey, value: newValue }
}

interface KeyValuePair {
	key: string
	value: any
}
