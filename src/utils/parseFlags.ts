export default function (value: number, length?: number): string[] {
	const flags = value.toString(16).split('').reverse()
	// return as new array of set length
	return [...Array(length || flags.length).fill('0')].map((val, idx) => flags[idx] || val)
}
