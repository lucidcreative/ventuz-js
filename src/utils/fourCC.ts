/**
 * intToFourCC
 * @description converts an integer into a FourCC identifier string
 * @param num Integer to convert to FourCC format
 * @returns {string} FourCC identifier
 */
export default function intToFourCC(num: number): string {
	let st = ''
	if (isNaN(num)) {
		throw ('Not a valid integer')
	}
	if (num === 0) {
		st = '0000'
	} else while (num > 0) {
		st += String.fromCharCode(num & 0xff)
		num = Math.floor(num / 256)
	}
	return st.split('').reverse().join('')
}
