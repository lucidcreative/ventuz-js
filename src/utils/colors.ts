import colornames from 'colornames'
import winston from 'winston'

const defaultColor = '127,127,127'

export function colorToRGBString(color: any): string {
	winston.debug(`converting ${color} to R,G,B String`)
	const re = RegExp(/\d+, ?\d+, ?\d+/)
	if (re.test(color)) {
		// color already in rgb format, so return
		return color
	} else {
		try {
			winston.debug(`Trying colornames on ${color}`)
			const hex = colornames(color)
			winston.debug(`colorToRGBString: ${color} became ${hex}`)
			const rgb = hexToRgb(hex)
			if (!rgb) { throw Error("Couldn't parse color name") }
			return `${rgb.r},${rgb.g},${rgb.b}`
		} catch (e) {
			winston.warn(`Caught error while trying to convert color:\n\t${e}`)
			return defaultColor
		}
	}
}

function hexToRgb(hex: any): any {
	const result = /^#?([a-f\d]{2})([a-f\d]{2})([a-f\d]{2})$/i.exec(hex)
	if (!result) { throw Error(`Could not parse hex value from ${hex}`) }
	return {
		r: parseInt(result[1], 16),
		g: parseInt(result[2], 16),
		b: parseInt(result[3], 16),
	}
}
