/**
 * transports.ts
 * @author KBjordahl
 * @description functions for sending data to a ventuz instance
 */

import axios from 'axios'
import fetch from 'node-fetch'
import * as querystring from 'querystring'
// import osc from 'ventuz-osc';
import * as R from 'ramda'
import { inspect } from 'util'
import winston from 'winston'
// winston.level = 'debug';

import { VMS_PORT } from '../constants'
// annoying form-data fix
const FormData = require('form-data')

// ventuz requires multipart/form-data
axios.defaults.headers.post['Content-Type'] = 'multipart/form-data'
axios.defaults.headers.get['Accept'] = 'application/json'

/**
 * sendHttp sends an api command to a ventuz instance using HTTP Remoting. This is ASYNC
 * @param ip Ventuz instance IP
 * @param commandPath Path to the command, starting with '/'
 * @param queryArgs an array of arguments for the command, in order
 * @param rawResponse whether or not to send the raw axios response
 * @param axiosConfig an object to merge into the axios config for the request
 */
export async function httpRemotingGet(ip: string, commandPath: string, queryArgs?: any[], rawResponse: boolean = false, axiosConfig?: any) {
	winston.debug('Query arguments sent to httpRemotingGet: ', queryArgs)
	// url for this command in HTTP Remoting
	const fullUrl = buildRemotingUrl(ip, commandPath) + encodeQueryArgs(queryArgs)
	const config = { headers: { 'Accept': 'application/json' }, ...axiosConfig }

	winston.debug(`sending via http: ${fullUrl}`)
	const response = await axios.get(fullUrl, config)
	winston.debug(`Transport got: ${inspect(response)}`)

	if (rawResponse) {
		winston.debug('Transport returning raw response.')
		return response
	} else {
		return handleResponse(response)
	}
}

export async function httpRemotingPost(ip: string, commandPath: string, args: any[] = [], returnRawResponse: boolean = false): Promise<any> {
	const url = buildRemotingUrl(ip, commandPath)
	const form = new FormData()

	// unfortunately FormData requires us to mutate it, so we do a stupid loop
	for (const arg of args) {
		form.append('', arg)
	}
	winston.debug('sending via http-POST: ', { url, form })
	// manually promisify; form - data submit function
	// const response = await new Promise(
	//     (resolve, reject) => form.submit(url, (error: any, result: any) => error ? reject(error) : resolve(result)));
	// winston.debug(`Post Transport got: ${inspect(response)}`);

	const response = await fetch(url, { method: 'POST', body: form })

	if (returnRawResponse) {
		winston.debug('Transport returning raw response.')
		return response
	} else {
		return response.json()
	}
}

export async function vmsGet(
	ip: string,
	commandPath: string,
	args: any[] | {} = [],
	returnRawResponse: boolean = false,
) {

	const fullUrl = buildVmsUrl(ip, commandPath) + encodeQueryArgs(args)
	const config = { headers: { 'Accept': 'application/json' } }

	winston.debug(`sending via http: ${fullUrl}`)
	const response = await axios.get(fullUrl, config)
	winston.debug(`Transport got: ${inspect(response)}`)

	if (returnRawResponse) {
		winston.debug('Transport returning raw response.')
		return response
	} else {
		return handleResponse(response)
	}
}
export function buildRemotingUrl(ip: string, commandPath: string): string {
	return ip.startsWith('http') ?
		`${ip}/remoting/1.0/Remoting4${commandPath}`
		: `http://${ip}:${VMS_PORT}/remoting/1.0/Remoting4${commandPath}`
}

export function buildVmsUrl(ip: string, commandPath: string): string {
	return ip.startsWith('http') ?
		`${ip}/${commandPath}`
		: `http://${ip}:${VMS_PORT}/${commandPath}`
}

export function encodeQueryArgs(args?: any[] | {}): string {
	// if the args are an array, then pack them, otherwise extract key/value pair and pack them together
	const encodeArgsForUri: any = Array.isArray(args) ?
		R.map((arg: any) => { return querystring.escape(arg) }) :
		R.mapObjIndexed((v: string, k: string) => `${k}=${querystring.escape(v)}`)
	const query = args ? R.join('&', encodeArgsForUri(args)) : ''
	winston.debug('Query string is:', query)
	return query.length > 0 ? `?${query}` : ''
}

function handleResponse(response: any) {
	if (R.has('data', response)) {
		const data = R.prop('data', response)
		if (R.has('Error', data)) {
			winston.error('Transport got error from Ventuz', data)
			throw (data)
		} else {
			winston.debug('Transport resolving with data', data)
			return data
		}
	} else {
		winston.error('Transport got error attempting to reach Ventuz', response)
		throw (response)
	}
}
// export async function sendOsc(ip: string, address: string, arguments?: string[], callback: Function) {
// }

export default {
	httpRemotingGet,
	httpRemotingPost,
	buildRemotingUrl,
}
