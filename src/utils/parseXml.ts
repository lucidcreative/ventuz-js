// /**
//  * @name Parse XML
//  * @description Handles uniform parsing of XML, returns Maybe monad
//  * @argument {string} rawXML - the xml data to parse
//  * @sig String -> Object
//  * @returns {Object} JSON parsed XML object
//  */

//  import * as fastXmlParser from 'fast-xml-parser';

// export function parseXML(rawXML: string) {
//     return fastXmlParser.parse(rawXML, {
//         attributeNamePrefix : '',
//         // attrNodeName: "attr", //default is 'false'
//         textNodeName : '#text',
//         ignoreAttributes : false,
//         ignoreNameSpace : false,
//         allowBooleanAttributes : false,
//         parseNodeValue : true,
//         parseAttributeValue : true,
//         trimValues: true,
//         cdataTagName: '__cdata', // default is 'false'
//         cdataPositionChar: '\\c'
//     });
// }

// export default parseXML;
