from bs4 import BeautifulSoup
import json
import re
import os
from pprint import pprint
soup = BeautifulSoup(open(".\\ventuz_docs.html"), "html.parser")

def clean_formatting(string):
    return re.sub(r'\s\s+'," ",string)

def makeFirstLower(string):
    return string[0].lower() + string[1:]

OSC_TYPES = dict(
    i="Number",
    h="Number",
    t="Number",
    s="string",
    S="Object",
    f="Number",
    d="Number",
    I="Boolean",
    b="ByteArray",
    B="BitVector",
    T="Boolean=true",
    F="Boolean=false",
    N="undefined"
)

commands = soup('h4')
c = []
for command in commands:
    item = dict()
    path = command.find_next('pre')
    short = path.find_next('em')
    description = short.find_next('p')
    category = command.find_previous('h3')

    # determine path arguments
    args = []
    path_components = unicode(path.string).split("/")[1:]
    for node in path_components:
        if node.startswith("<") and node.endswith(">"):
            args.append(dict(
                name=node[1:-1], 
                type="string", 
                js_type='string', 
                description="",
                optional=False,
                path=True,
                array=False,
                ))

    # Do Arguments
    arg_data = command.find_next("span", string="Request").find_next("table").find_all('tr')
    for arg in arg_data:
        # import pdb; pdb.set_trace()
        # if there's only header cells, skip this row
        if len(arg('td')) == 0 : continue

        a=None
        js_type = 'any'
        osc_type = ''
        optional = False
        
        type_data = arg.find('tt')

        osc_type = unicode(type_data.string)
        if osc_type in ['i', 'h', 'f', 'd']: js_type = 'number'
        elif osc_type == "s": js_type = "string"
        elif osc_type == "S": js_type = "Object"
        elif osc_type == 'N': js_type = "undefined"
        elif osc_type == "T": js_type = "Boolean"
        elif osc_type == "F": js_type = "Boolean"
        elif osc_type == "B": js_type = "Array<boolean>"
        elif osc_type == "b": js_type = "ByteArray"
        
        if "[" in osc_type: array=True 
        else: array=False 

        # is it optional?
        if type_data.next_sibling.string == "?":
            optional = True
        else: optional = False

        # try:
        #     type = " ".join(arg('th')[0].stripped_strings)
        # except:
        #     type = ""

        a = dict(
            type=js_type,
            name=" ".join(arg('td')[0].stripped_strings),
            description=clean_formatting(" ".join(arg('td')[1].stripped_strings)),
            path=False,
            optional=optional,
            array=array,
        )
        args.append(a)
       


    result_data = command.find_next("span", string="Response").find_next('table').find_all('tr')
    results = []
    for result in result_data:
        # confirm the row has data cells, if not, skip
        if len(result('td')) == 0 : continue

        try:
            type = " ".join(result('th')[0].stripped_strings)
        except:
            type = ""

        r = dict(
            type=type,
            name=" ".join(result('td')[0].stripped_strings),
            description=clean_formatting(" ".join(result('td')[1].stripped_strings)),
        )
        results.append(r)

    item['id'] = command['id']
    # split async off name
    split_name = unicode(command.string).split(" ", 1)
    item['name'] = split_name[0]
    try:
        item['async'] = split_name[1] and 'async' in split_name[1] 
    except:
        item['async'] = False
    item['path'] = unicode(path.string).strip()
    item['short'] = " ".join(short.stripped_strings)
    item['description'] = clean_formatting(" ".join(description.stripped_strings))
    item['arguments'] = args
    item['results'] = results
    item['category'] = dict(
        id=category['id'],
        name=unicode(category.string)
    )
    
    assert len(item['path']) > 0, "Path must be greater than 0"
    assert len(item['short']) > 0, "Must have short description"
    assert len(item['description']) > 0, "Must have description"

    c.append(item)

with open('ventuz_remoting.json', 'w') as op:
    json.dump(c,op)

# now let's get stupid. Make some TS files from Python!

for command in c:
    # arg_list = []
    # for arg in command['args']:
    #     arg_list.append(
    #         "  * @argument {a[name]} {{{a[type]}}}: {a[description]}"
    #     )
    args = command['arguments']
    to_remove = []
    for index, arg in enumerate(args):
        args[index]['name'] = makeFirstLower(arg['name'])
        # ditch request ID
        if "id" in arg['description'].lower() and "request" in arg['description'].lower(): 
            to_remove.append(arg)

    for removal in to_remove:
        args.remove(removal)

    args_doc = "\n".join([
        " * @param {{{a[type]}}} {a[name]}: {a[description]}".format(a=arg).replace(" ?", "| undefined")
        for arg in args
        ])

    args_string = ', '.join([
        "{a[name]}: {a[type]}{array}{opt}".format(
            a=arg,  
            opt=" | undefined" if arg['optional'] else "",
            array="[]" if arg['array'] else "",
            )
        for arg in args
        ])

    query_args = ',\n\t'.join([arg['name'] for arg in args if not arg['path'] ])

    command['id'] = makeFirstLower(command['id'])

    # convert the command path to work with js template literals
    mod_path = command['path'].replace("<","${").replace(">","{")    
    
    js_code = '''
'use strict'
/**
 * @name {c[name]} ({c[path]})
 * @function {c[id]}
 * @description **{c[short]}.** {c[description]}
 * @author KBjordahl
 * @param {{string}} ip: IP address of Ventuz
{arg_doc}
 */

// osc async = {c[async]}

import {{ sendHttp }} from '../../utils/transport'

export default async function {c[id]}(ip: string, {args}){{
    const path = '{path}';
    const queryArgs = [
        {query_args}
    ];
    const response = await sendHttp(ip, path, queryArgs));
    return response;
}}'''.format(
        c=command, 
        arg_doc=args_doc, 
        args=args_string, 
        path=mod_path,
        query_args=query_args,
        )

    print js_code
    path = os.path.join('.','src','remoting', command['category']['id'])
    try:
        os.makedirs(path)
    except:
        pass

    file = os.path.join(path, command['id']+".ts")
    with open(file, 'w') as js:
        js.write(js_code)
