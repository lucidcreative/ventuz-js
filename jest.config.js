// module.exports = {
// 	globals: {
// 		'ts-jest': {
// 			tsConfigFile: 'tsconfig.json'
// 		}
// 	},
// 	moduleFileExtensions: [
// 		'ts',
// 		'js'
// 	],
// 	transform: {
// 		'^.+\\.(ts|tsx)$': './node_modules/ts-jest/preprocessor.js'
// 	},
// 	testMatch: [
// 		'**/test/**/*.test.(ts|js)'
// 	],
// 	testEnvironment: 'node'
// };
module.exports = {
	transform: {
		"^.+\\.tsx?$": "ts-jest",
	},
	moduleNameMapper: {
		'^@/(.*)$': '<rootDir>/src/$1',
	},
	globals: {
		'ts-jest': {
			tsConfig: '<rootDir>/tsconfig.json'
		}
	},
	testRegex: "(/src/.*/__test__/.*|(\\.|/)(test|spec))\\.(jsx?|tsx?)$",
	testPathIgnorePatterns: ["/lib/", "/node_modules/"],
	// setupTestFrameworkScriptFile: "/test/setupTestEnvironment.ts",
	moduleFileExtensions: [
		"ts",
		"tsx",
		"js",
		"jsx",
		"json",
		"node"
	],
	collectCoverage: false,
};